package com.bektursun.fitnestracker.util

const val APP_LOG = "AppLog"

val ALL_APP_PERMISSIONS = arrayOf(
        android.Manifest.permission.ACCESS_FINE_LOCATION,
        android.Manifest.permission.ACCESS_COARSE_LOCATION)

const val ALL_PERMISSION_CODE = 11

const val DATABASE_NAME = "appdatabase"

