package com.bektursun.fitnestracker.util.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.bektursun.fitnestracker.R
import com.bektursun.fitnestracker.repository.db.model.Track
import com.bektursun.fitnestracker.util.getTime
import kotlinx.android.synthetic.main.dialog_jogging_detail.*

class JoggingDetailFragment : DialogFragment() {

    lateinit var track: Track

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_jogging_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtStartTime.text = "Starat ${getTime(track.startTime)}"

        txtAvgSpeed.text = "Avg.speed:${track.avgSpeed}m/s"
        txtMaxSpeed.text = "Max speed:${track.maxSpeed}m/s"
        txtMinSpeed.text = "Min speed:${track.minSpeed}m/s"

        txtDistance.text = "Distance:${track.distance.toInt()}m"
        txtAccuracy.text = "Accuracy:${track.accuracy.toInt()}%"
    }

}