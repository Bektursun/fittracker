package com.bektursun.fitnestracker.repository

import android.annotation.SuppressLint
import com.bektursun.fitnestracker.repository.db.AppDatabase
import com.bektursun.fitnestracker.repository.db.model.LocationUpdate
import com.bektursun.fitnestracker.repository.db.model.Track
import com.bektursun.fitnestracker.ui.callback.MapCallback
import com.bektursun.fitnestracker.ui.callback.TracksCallback
import com.bektursun.fitnestracker.util.IO
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DataManager(val db: AppDatabase) {
    val locationsUpdates = mutableListOf<LocationUpdate>()
    @SuppressLint("CheckResult")
    fun loadTracks(callback: TracksCallback) {
        db.trackDao().getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    callback.onTracksLoaded(it)
                }, {
                    callback.error(it)
                })
    }


    @SuppressLint("CheckResult")
    fun loadLocations(callback: MapCallback, trackId: Long) {
        db.locationUpdateDao().getAll(trackId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    locationsUpdates.clear()
                    locationsUpdates.addAll(it)
                    callback.onLocationsLoaded(it)
                }, {
                    callback.error(it)
                })
    }

    fun updateTrack(trackId: Long, startTime: Long, endTime: Long,
                    elapsedTime: Long,
                    minSpeed: Float,
                    maxSpeed: Float,
                    avgSpeed: Float, accuracy: Float, distance: Double) {
        val track = Track(
                id = trackId,
                startTime = startTime,
                endTime = endTime,
                minSpeed = minSpeed,
                maxSpeed = maxSpeed,
                avgSpeed = avgSpeed,
                accuracy = accuracy,
                distance = distance,
                elapsedTime = elapsedTime)
        IO.execute {
            db.trackDao().insert(track)
        }
    }

    fun clear() {
        locationsUpdates.clear()
    }
}