package com.bektursun.fitnestracker.repository.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "track")
class Track(
        @PrimaryKey
        @ColumnInfo(name = "id")
        val id: Long,
        @ColumnInfo(name = "elapsed")
        val elapsedTime: Long,
        @ColumnInfo(name = "start")
        val startTime: Long,
        @ColumnInfo(name = "end")
        val endTime: Long,
        @ColumnInfo(name = "avg_speed")
        val avgSpeed: Float,
        @ColumnInfo(name = "min_speed")
        val minSpeed: Float,
        @ColumnInfo(name = "max_speed")
        val maxSpeed: Float,
        @ColumnInfo(name = "distance")
        val distance: Double,
        @ColumnInfo(name = "accuracy")
        val accuracy: Float) : Serializable