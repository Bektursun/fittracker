package com.bektursun.fitnestracker.repository.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bektursun.fitnestracker.repository.db.model.Track
import io.reactivex.Single

@Dao
interface TrackDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(track: Track)

    @Query("SELECT * FROM track")
    fun getAll(): Single<List<Track>>

}