package com.bektursun.fitnestracker.repository.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bektursun.fitnestracker.repository.db.model.LocationUpdate
import io.reactivex.Single

@Dao
interface LocationUpdateDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(obj: LocationUpdate)

    @Query("SELECT * FROM location WHERE track_id = :trackId")
    fun getAll(trackId: Long): Single<List<LocationUpdate>>


}