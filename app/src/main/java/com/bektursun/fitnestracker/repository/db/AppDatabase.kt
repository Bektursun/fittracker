package com.bektursun.fitnestracker.repository.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.bektursun.fitnestracker.repository.db.dao.LocationUpdateDao
import com.bektursun.fitnestracker.repository.db.dao.TrackDao
import com.bektursun.fitnestracker.repository.db.model.LocationUpdate
import com.bektursun.fitnestracker.repository.db.model.Track
import com.bektursun.fitnestracker.util.DATABASE_NAME

@Database(entities = [Track::class, LocationUpdate::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun trackDao(): TrackDao

    abstract fun locationUpdateDao(): LocationUpdateDao


    companion object {

        // For Singleton instantiation
        @Volatile
        private var instance: AppDatabase? = null

        fun dbInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance
                        ?: buildDatabase(context).also {
                    instance = it
                }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME).build()
        }
    }




}