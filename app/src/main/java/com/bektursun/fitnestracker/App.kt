package com.bektursun.fitnestracker

import android.app.Application
import com.bektursun.fitnestracker.repository.db.AppDatabase

class App : Application() {

    lateinit var db: AppDatabase
    lateinit var dm: com.bektursun.fitnestracker.repository.DataManager

    override fun onCreate() {
        super.onCreate()
        db = AppDatabase.dbInstance(this)
        dm = com.bektursun.fitnestracker.repository.DataManager(db)

    }
}