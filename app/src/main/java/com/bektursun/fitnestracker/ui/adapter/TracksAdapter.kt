package com.bektursun.fitnestracker.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bektursun.fitnestracker.R
import com.bektursun.fitnestracker.repository.db.model.Track
import com.bektursun.fitnestracker.util.calculateInMinutes
import com.bektursun.fitnestracker.util.getTime
import kotlinx.android.synthetic.main.list_item_track.view.*
import java.text.DecimalFormat


class TracksAdapter : RecyclerView.Adapter<TracksAdapter.ViewHolder>() {
    val list = mutableListOf<Track>()


    fun addAll(newList: List<Track>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_track, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(track: Track) {

            itemView.txtStartTime.text = "Start time ${getTime(track.startTime)}"
            itemView.txtEndTime.text = "End time ${getTime(track.endTime)}"
            itemView.txtSpendTime.text = "Time ${calculateInMinutes(track.elapsedTime)}"

            itemView.txtAvgSpeed.text = "Avg.speed:${DecimalFormat("##.##").format(track.avgSpeed)}m/s"
            itemView.txtMaxSpeed.text = "Max speed:${DecimalFormat("##.##").format(track.maxSpeed)}m/s"
            itemView.txtMinSpeed.text = "Min speed:${DecimalFormat("##.##").format(track.minSpeed)}m/s"

            itemView.txtDistance.text = "Distance:${track.distance.toInt()}m"
            itemView.txtAccuracy.text = "Accuracy:${track.accuracy.toInt()}%"

        }
    }

}