package com.bektursun.fitnestracker.ui.activity

import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bektursun.fitnestracker.R
import com.bektursun.fitnestracker.repository.db.model.Track
import com.bektursun.fitnestracker.ui.adapter.TracksAdapter
import com.bektursun.fitnestracker.ui.callback.TracksCallback
import com.bektursun.fitnestracker.util.*
import kotlinx.android.synthetic.main.activity_tracks.*

class TracksActivity : AppCompatActivity(), TracksCallback {

    lateinit var adapter: TracksAdapter
    lateinit var dm: com.bektursun.fitnestracker.repository.DataManager
    var location: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tracks)
        dm = applicationContext.dm()
        val id = applicationContext.getId()
        if (id != -1L) {
            val latlng = applicationContext.getLatLng()
            startMapActivity(id, latlng.latitude, latlng.longitude)
        }

        if (!PermissionUtil.checkPermissions(this)) {
            PermissionUtil.requestPermissions(this)
        } else {
            getLastLocation()
        }

        adapter = TracksAdapter()
        recyclerview.adapter = adapter

        dm.loadTracks(this)

        fabStart.setOnClickListener {
            if (PermissionUtil.isAccessFineGranted(this)) {
                if (location != null) {
                    val trackId = System.currentTimeMillis()
                    applicationContext.putId(trackId)
                    startMapActivity(trackId, location!!.latitude, location!!.longitude)
                } else {
                    getLastLocation(true)
                }
            } else {
                showMessage("Permission denied")
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == ALL_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation()
            }
        }
    }

    fun getLastLocation(startMapActivity: Boolean = false) {
        SingleLocationProvider.requestLocation(this, object : SingleLocationProvider.LocationCallback {
            override fun onLocationChanged(location: Location) {
                if (startMapActivity) {
                    val trackId = System.currentTimeMillis()
                    applicationContext.putId(trackId)
                    startMapActivity(trackId, location.latitude, location.longitude)
                } else
                    this@TracksActivity.location = location
            }
        })
    }



    override fun onTracksLoaded(list: List<Track>) {
        adapter.addAll(list)
    }

    override fun error(e: Throwable) {
        Toast.makeText(this, e.localizedMessage, Toast.LENGTH_LONG).show()
    }

}
