package com.bektursun.fitnestracker.ui.callback

import com.bektursun.fitnestracker.repository.db.model.Track

interface TracksCallback : BaseCallback {
    fun onTracksLoaded(list: List<Track>)
}