package com.bektursun.fitnestracker.ui.callback

import com.bektursun.fitnestracker.repository.db.model.LocationUpdate

interface MapCallback :BaseCallback{
    fun onLocationsLoaded(list: List<LocationUpdate>)
}