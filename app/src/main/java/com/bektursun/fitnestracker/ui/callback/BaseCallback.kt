package com.bektursun.fitnestracker.ui.callback


interface BaseCallback {
    fun error(e: Throwable)
}