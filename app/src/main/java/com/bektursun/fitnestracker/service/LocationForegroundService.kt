package com.bektursun.fitnestracker.service

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.location.Location
import android.os.*
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bektursun.fitnestracker.repository.db.dao.LocationUpdateDao
import com.bektursun.fitnestracker.util.*
import com.google.android.gms.location.*



class LocationForegroundService : Service() {

    private val CHANNEL_ID = "channel_01"

    companion object {
        private val PACKAGE_NAME = "io.maddev.fitnestracker"

        val EXTRA_LOCATION = "$PACKAGE_NAME.location"

        val ACTION_BROADCAST = "$PACKAGE_NAME.broadcast"

    }

    private val binder = LocalBinder()

    private val EXTRA_STARTED_FROM_NOTIFICATION = "$PACKAGE_NAME.started_from_notification"

    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 0

    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2

    private val NOTIFICATION_ID = 87654

    private var changingConfiguration = false

    private var notificationManager: NotificationManager? = null

    private var locationRequest: LocationRequest? = null

    private var fusedLocationClient: FusedLocationProviderClient? = null

    private var locationCallback: LocationCallback? = null

    private var serviceHandler: Handler? = null

    private var location: Location? = null

    var trackId = -1L

    private lateinit var dao: LocationUpdateDao

    override fun onCreate() {
        super.onCreate()

        dao = applicationContext.db().locationUpdateDao()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)


        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                onNewLocation(locationResult!!.lastLocation)
            }
        }
        createLocationRequest()
        getLastLocation()


        val handlerThread = HandlerThread(LocationForegroundService::class.java.simpleName)
        handlerThread.start()
        serviceHandler = Handler(handlerThread.looper)
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(com.bektursun.fitnestracker.R.string.app_name)
            val mChannel = NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT)

            notificationManager?.createNotificationChannel(mChannel)
        }

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val startedFromNotification = intent?.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION,
                false) ?: false

        if (startedFromNotification) {
            removeLocationUpdates()
            stopSelf()
        }
        return Service.START_NOT_STICKY


    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        changingConfiguration = true
    }


    override fun onBind(intent: Intent): IBinder? {
        stopForeground(true)
        changingConfiguration = false
        return binder
    }

    override fun onRebind(intent: Intent) {
        stopForeground(true)
        changingConfiguration = false
        super.onRebind(intent)
    }


    override fun onUnbind(intent: Intent): Boolean {
        if (!changingConfiguration && LocationServiceUtil.requestingLocationUpdates(this)) {
            startForeground(NOTIFICATION_ID, getNotification())
        }
        return true
    }

    override fun onDestroy() {
        serviceHandler?.removeCallbacksAndMessages(null)
    }


    fun requestLocationUpdates() {
        LocationServiceUtil.setRequestingLocationUpdates(this, true)
        startService(Intent(applicationContext, LocationForegroundService::class.java))
        try {
            fusedLocationClient?.requestLocationUpdates(locationRequest,
                    locationCallback, Looper.myLooper())
        } catch (unlikely: SecurityException) {
            LocationServiceUtil.setRequestingLocationUpdates(this, false)
        }

    }


    fun removeLocationUpdates() {
        try {
            fusedLocationClient?.removeLocationUpdates(locationCallback)
            LocationServiceUtil.setRequestingLocationUpdates(this, false)
            stopSelf()
        } catch (unlikely: SecurityException) {
            LocationServiceUtil.setRequestingLocationUpdates(this, true)
        }
    }


    private fun getNotification(): Notification {
        val intent = Intent(this, LocationForegroundService::class.java)

        val text = LocationServiceUtil.getLocationText(location)

        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true)

        val servicePendingIntent = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(this)
                .addAction(com.bektursun.fitnestracker.R.drawable.ic_cancel, getString(com.bektursun.fitnestracker.R.string.remove_location_updates),
                        servicePendingIntent)
                .setContentText(text)
                .setContentTitle(LocationServiceUtil.getLocationTitle(this))
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(com.bektursun.fitnestracker.R.mipmap.ic_launcher)
                .setTicker(text)
                .setWhen(System.currentTimeMillis())

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID) // Channel ID
        }

        return builder.build()
    }

    private fun getLastLocation() {
        try {
            fusedLocationClient?.lastLocation
                    ?.addOnCompleteListener { task ->
                        if (task.isSuccessful && task.result != null) {
                            location = task.result
                        } else {
                        }
                    }
        } catch (unlikely: SecurityException) {
            Log.e(APP_LOG, "Lost location permission.$unlikely")
        }

    }


    @SuppressLint("CheckResult")
    fun onNewLocation(newLocation: Location) {

        location = newLocation
        save(newLocation)

        val intent = Intent(ACTION_BROADCAST)
        intent.putExtra(EXTRA_LOCATION, location)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)

        if (serviceIsRunningInForeground(this)) {
            notificationManager?.notify(NOTIFICATION_ID, getNotification())
        }

    }

    private fun save(newLocation: Location) {

        IO.execute {
            if (trackId == -1L)
                return@execute
            applicationContext.putLatLng(newLocation.latitude, newLocation.longitude)
            dao.insert(newLocation.toLocationUpdate(System.currentTimeMillis(), trackId))
        }

    }


    private fun createLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest?.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        locationRequest?.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }


    inner class LocalBinder : Binder() {
        internal val service: LocationForegroundService
            get() = this@LocationForegroundService
    }


    fun serviceIsRunningInForeground(context: Context): Boolean {
        val manager = context.getSystemService(
                Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (javaClass.name == service.service.className) {
                if (service.foreground) {
                    return true
                }
            }
        }
        return false
    }


}